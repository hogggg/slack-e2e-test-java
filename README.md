# Slack End to End Automation Test

## Description

##### This is a Java based end to end automation project to test Slack. It contains one simple message test scenario. In this project, [Cucumber testing tool](https://cucumber.io/tools/cucumberstudio/?utm_source=aw&utm_medium=ppcg&utm_term=cucumber%20automation&utm_content=444661858931&utm_campaign=10382370862&gclid=CjwKCAiAgJWABhArEiwAmNVTB4lQq0SErDE2HaoBwJLxNHtv36VVB-PmEBxLXkKQitldzzYTRs-NdRoCGqcQAvD_BwE&gclsrc=aw.ds) is used to define test scenario. A few API calls are made for data preparation and cleanup, [Rest-assured](https://rest-assured.io) framework is used to make API calls. The web automation test is written with [Selenium WebDriver](https://www.selenium.dev). Design patterns of Page Object Model and Factory are applied in this project. Furthermore, it can generate xml, html and json formats reports to support potential future needs.

---

## Prerequisites

Please ensure the following applications are installed before running tests:

1. Java SE Development Kit 8, [download and install first](https://www.oracle.com/ca-en/java/technologies/javase/javase-jdk8-downloads.html).
   
2. [Chrome](https://www.google.ca/chrome/?brand=JJTC&gclid=CjwKCAiAgJWABhArEiwAmNVTB48-8UuqJ9D8GsJ0o4uh99Zxp-AoQRCprSTtO9gXxZcEvBbyy6buaBoCmKQQAvD_BwE&gclsrc=aw.ds) and/or [Firefox](https://www.mozilla.org/en-CA/firefox/new/) is required to run web automation test in this project

2. Cucumber for Java plugin needs to be installed in the IDE
   
3. Lombok plugin is required for this project. Please install Lombok plugin in the IDE. Enable annotation processing from the IDE if needed.
   
```
As an example, see below steps to enable annotation processing from Intellij:
a. In the Settings/Preferences dialog, select Build, Execution, Deployment > Compiler > Annotation processors.
b. In the Annotation processors dialog, ensure the checkbox of option Enable annotation processing is checked.
```
![img.png](readmeImg/img.png)


---

## Install

**Option #1**: Download codes from the [repository](https://bitbucket.org/hogggg/slack-e2e-test/src/master/)

**Option #2**: Clone the repository from the terminal
```bash
git clone https://hogggg@bitbucket.org/hogggg/slack-e2e-test.git
```
Once the project is downloaded or cloned, you can import it as a Gradle project from your IDE.

---
## How to run test
This project supports run web tests from Chrome and Firefox for now. To change test configurations, simply find the configuration file **config.json** from the root directory of the project.

From the **config.json** file, the property **browserType** is defined for switching among web drivers. Meanwhile, you can also change the flag **headless** to be true to run tests without opening web browsers.

Once the configuration is done, you can simply run the web test from cucumber feature files, which locates in the resource folder of test module.

Alternatively, the entire test suite can be triggered from the cucumber runner class **CucumberTestRun.class**, which locates in the test module.

**_NOTE:_** when run the web test with webdriver (chromedriver and/or geckodriver), you may face the issue, which may say _Error: "chromedriver" can not be opened because the developer cannot be verified_.
To resolve it, go to the root directory of this project from the terminal, and run below commands:
```bash
cd dependencies/
xattr -d com.apple.quarantine chromedriver
xattr -d com.apple.quarantine geckodriver
```

---
## What is next
1. Find a better way to bypass user login. E.g. by injecting auth token into cookies. More options to be explored.
2. Add dependency injection for page object model and web driver loading. I used to use Spring framework's dependency injection to instantiate page object classes and inject the WebDriver. However, it brings more complexities to the test code. I find it is also heavy for the automated project. I didn't add Spring into this project because I'm looking for a much lighter framework to handle dependency injections. Due to the time constrains, I didn't add support to that. To be added very soon.
3. Test parallel execution. Since only one test is in this project at the moment, there is no need for parallel execution. To be added in the future.





