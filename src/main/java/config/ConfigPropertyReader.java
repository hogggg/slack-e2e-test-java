package config;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class ConfigPropertyReader {

    public static ConfigProperty getConfigProperty() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
        File file = new File("config.json");
        try {
            return mapper.readValue(file, ConfigProperty.class);
        } catch (IOException e) {
            e.printStackTrace();
            return new ConfigProperty();
        }
    }
}
