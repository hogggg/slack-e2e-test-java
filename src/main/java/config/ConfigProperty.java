package config;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import platform.web.driver_manager.DriverType;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigProperty {

    private RestProperties restProperties;
    private WebProperties webProperties;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class RestProperties {
        private String restBaseUrl;
        private String token;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class WebProperties {
        private DriverType browserType;
        private String browserVersion;
        private Boolean headless;
        private String downloadPath;
        private String chromeDriverPath;
        private String firefoxDriverPath;
        private String responsiveResolution;
        private String webBaseUrl;
        private String userName;
        private String password;
    }
}
