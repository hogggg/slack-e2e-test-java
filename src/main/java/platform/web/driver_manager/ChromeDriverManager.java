package platform.web.driver_manager;

import config.ConfigProperty;
import config.ConfigPropertyReader;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;

public class ChromeDriverManager extends DriverManager {

    private final ConfigProperty.WebProperties webProperties = ConfigPropertyReader.getConfigProperty().getWebProperties();
    private ChromeDriverService chService;

    @Override
    public void startService() {
        if (null == chService) {
            try {
                System.setProperty("webdriver.chrome.driver", webProperties.getChromeDriverPath());
                chService = new ChromeDriverService.Builder()
                        .usingAnyFreePort()
                        .build();
                chService.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void createDriver() {

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", webProperties.getBrowserVersion());

        final HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", webProperties.getDownloadPath());

        final ChromeOptions options = new ChromeOptions();
        if(webProperties.getHeadless().equals(true)){
            options.addArguments("--headless");
        }
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("--test-type");
        options.addArguments("--disable-extensions");
        options.merge(caps);

        final int resolutionVertical = Integer.parseInt(webProperties.getResponsiveResolution().split("x")[1]);
        final int resolutionHorizontal = Integer.parseInt(webProperties.getResponsiveResolution().split("x")[0]);
        driver = new ChromeDriver(options);
        driver.manage().window().setSize(new Dimension(resolutionVertical, resolutionHorizontal));
        driver.get(webProperties.getWebBaseUrl());
    }
}
