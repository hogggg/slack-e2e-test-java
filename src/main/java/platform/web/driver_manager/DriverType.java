package platform.web.driver_manager;

public enum DriverType {
    CHROME,
    FIREFOX;
}
