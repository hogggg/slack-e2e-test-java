package platform.web.driver_manager;

import config.ConfigProperty;
import config.ConfigPropertyReader;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class FirefoxDriverManager extends DriverManager {
    private final ConfigProperty.WebProperties webProperties = ConfigPropertyReader.getConfigProperty().getWebProperties();
    private GeckoDriverService ffService;

    @Override
    public void startService() {
        if (null == ffService) {
            try {
                System.setProperty("webdriver.gecko.driver", webProperties.getFirefoxDriverPath());
                ffService = new GeckoDriverService.Builder()
                        .usingAnyFreePort()
                        .build();
                ffService.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void createDriver() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Firefox");
        caps.setCapability("browser_version", webProperties.getBrowserVersion());

        FirefoxOptions options = new FirefoxOptions();
        if(webProperties.getHeadless().equals(true)){
            options.addArguments("--headless");
        }
        options.addPreference("network.proxy.type", 0);
        options.merge(caps);

        final int resolutionVertical = Integer.parseInt(webProperties.getResponsiveResolution().split("x")[1]);
        final int resolutionHorizontal = Integer.parseInt(webProperties.getResponsiveResolution().split("x")[0]);
        driver = new FirefoxDriver(options);
        driver.manage().window().setSize(new Dimension(resolutionVertical, resolutionHorizontal));
        driver.get(webProperties.getWebBaseUrl());
    }
}
