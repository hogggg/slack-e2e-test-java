package platform.web.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static platform.web.page.PageUtil.waitForElementVisible;

public class SavedMessagePage {

    @FindBy(css = "div[data-qa = 'block-kit-renderer']")
    protected WebElement message_item;

    private final WebDriver driver;

    public SavedMessagePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getSavedMessage() {
        try {
            waitForElementVisible(driver, message_item);
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            waitForElementVisible(driver, message_item);
        }
        return message_item.getText();
    }
}
