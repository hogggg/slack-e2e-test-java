package platform.web.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class PageUtil {

    public static void fillTextInputField(WebElement textInputWebElement, String userInput) {
        textInputWebElement.clear();
        textInputWebElement.sendKeys(userInput);
    }

    public static void waitForElementVisible(WebDriver driver, WebElement element) {
        (new WebDriverWait(driver, Duration.ofSeconds(60), Duration.ofSeconds(5)))
                .until(ExpectedConditions.visibilityOf(element));
    }
}
