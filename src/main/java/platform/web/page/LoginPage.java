package platform.web.page;

import config.ConfigProperty;
import config.ConfigPropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.ArrayList;

import static platform.web.page.PageUtil.fillTextInputField;

public class LoginPage {

    @FindBy(css = "input[data-qa = 'login_email']")
    protected WebElement login_email_field;

    @FindBy(css = "input[data-qa = 'login_password']")
    protected WebElement login_password_field;

    @FindBy(css = "button[data-qa = 'signin_button']")
    protected WebElement signIn_button;

    @FindBy(css = "button[data-qa = 'close_fullscreen_modal']")
    protected WebElement close_ad_button;

    private final WebDriver driver;

    private final ConfigProperty.WebProperties webProperties = ConfigPropertyReader.getConfigProperty().getWebProperties();

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void loginToSlack() {
        fillTextInputField(login_email_field, webProperties.getUserName());
        fillTextInputField(login_password_field, webProperties.getPassword());
        signIn_button.click();

        //todo-hog find a better way to dismiss the browser alert
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.get(webProperties.getWebBaseUrl());


        String adDownloadButtonXpath = "//a[@data-qa = 'download_slack']";
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        boolean isPresent = !driver.findElements(By.xpath(adDownloadButtonXpath)).isEmpty();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(0));
        if (isPresent) {
            close_ad_button.click();
        }
    }

}
