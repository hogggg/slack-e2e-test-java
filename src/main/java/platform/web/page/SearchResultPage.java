package platform.web.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static platform.web.page.PageUtil.waitForElementVisible;

public class SearchResultPage {

    @FindBy(css = "div[data-qa = 'search_message_body']")
    protected WebElement searched_message_item;

    private final WebDriver driver;

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getSearchedMessage() {
        waitForElementVisible(driver, searched_message_item);
        return searched_message_item.getText();
    }
}
