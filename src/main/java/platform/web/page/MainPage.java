package platform.web.page;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;

import static platform.web.page.PageUtil.waitForElementVisible;


public class MainPage {
    @FindBy(css = "div[data-qa = 'message_input']>div")
    protected WebElement message_input;

    @FindBy(css = "div[data-qa = 'virtual-list-item']")
    protected WebElement message_list_day_divider;

    @FindBy(css = "button[data-qa = 'save_message']")
    protected WebElement message_save_button;

    @FindBy(css = "button[data-qa = 'top_nav_search']")
    protected WebElement search_bar;

    @FindBy(css = "div[data-qa = 'focusable_search_input']>div")
    protected WebElement search_field;

    @FindBy(css = "span[data-qa = 'channel_sidebar_name_page_psaved']")
    protected WebElement save_items_menu;

    private final WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void sendMessageToChannel(String message) {
        waitForElementVisible(driver, message_input);
        message_input.sendKeys(message);
        message_input.sendKeys(Keys.ENTER);
    }

    public void searchMessage(String message) {
        search_bar.click();
        search_field.sendKeys(message);
        search_field.sendKeys(Keys.ENTER);
        //this is a workaround due to the bug: message is returned as a delay in the search result page
        new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(5))
                .until((driver) -> {
                    try {
                        return driver.findElement(By.xpath("//div[@data-qa = 'search_message_group']"));
                    } catch (NoSuchElementException e) {
                        search_field.sendKeys(Keys.ENTER);
                        return null;
                    }
                });
    }

    public void saveMessage(String message) {
        waitForElementVisible(driver, message_list_day_divider);
        Actions action = new Actions(driver);
        String certainMessageXpath = "//div[contains(text(), '%s')]";
        try {
            WebElement myMessage = driver.findElement(By.xpath(String.format(certainMessageXpath, message)));
            action.moveToElement(myMessage).build().perform();
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            WebElement myMessage = driver.findElement(By.xpath(String.format(certainMessageXpath, message)));
            action.moveToElement(myMessage).build().perform();
        }
        waitForElementVisible(driver, message_save_button);
        message_save_button.click();
    }

    public void goToSaveItemsList() {
        waitForElementVisible(driver, save_items_menu);
        save_items_menu.click();
    }
}
