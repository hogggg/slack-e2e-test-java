package platform.rest;

import io.restassured.response.Response;
import platform.rest.request.MessageRequests;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageRequestsService {

    private final MessageRequests messageRequests = new MessageRequests();

    public Response getChannelListFromSlackTeam() {

        return messageRequests.getAllChannelsFromSlackTeam();
    }

    public List<HashMap> retrieveChannelChats(String channelId) {
        Map<String, String> formParams = new HashMap<>();
        formParams.put("channel", channelId);
        return messageRequests.retrieveChannelHistory(formParams).jsonPath().getList("messages", HashMap.class);
    }

    public void deleteChannelChat(String channelId, String chatTs) {
        Map<String, String> formParams = new HashMap<>();
        formParams.put("channel", channelId);
        formParams.put("ts", chatTs);
        messageRequests.deleteChannelMessage(formParams);
    }
}
