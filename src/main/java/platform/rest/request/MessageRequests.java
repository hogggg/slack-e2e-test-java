package platform.rest.request;

import config.ConfigProperty;
import config.ConfigPropertyReader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;

public class MessageRequests extends AbstractRequests {

    public Response getAllChannelsFromSlackTeam() {
        return getDefaultRestCall("/api/conversations.list", ContentType.JSON, is(HttpStatus.SC_OK), headerParamsMap());
    }

    public Response retrieveChannelHistory(Map<String, String> body) {
        return postDefaultRestCallWithFormData("/api/conversations.history", headerParamsMap(), body, is(HttpStatus.SC_OK));
    }

    public void deleteChannelMessage(Map<String, String> body) {
        postDefaultRestCallWithFormData("/api/chat.delete", headerParamsMap(), body, is(HttpStatus.SC_OK));
    }

    private Map<String, Object> headerParamsMap() {
        Map<String, Object> headerParams = new HashMap<>();
        ConfigProperty.RestProperties restProperties = ConfigPropertyReader.getConfigProperty().getRestProperties();
        headerParams.put("Authorization", String.format("Bearer %s", restProperties.getToken()));
        return headerParams;
    }
}
