package platform.rest.request;

import config.ConfigProperty;
import config.ConfigPropertyReader;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;


import java.io.PrintStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;

import static io.restassured.RestAssured.given;

public abstract class AbstractRequests {

    private static final Logger logger = LogManager.getLogger(AbstractRequests.class);

    private PrintStream printStream;
    private StringWriter stringWriter;

    /**
     * Builds a basic GET rest call with given url
     *
     * @param url         Request URL
     * @param contentType Request contentType
     * @param statusCode  Response expected status code
     * @param headers     Request headers
     * @return {@link Response} response
     */
    protected Response getDefaultRestCall(String url, ContentType contentType,
                                          Matcher<Integer> statusCode, Map<String, Object> headers) {
        setupRestAssureLogCapture();

        Response response = given()
                .config(RestAssured.config().logConfig(new LogConfig(printStream, true)))
                .spec(buildRequestBaseUri())
                .contentType(contentType)
                .headers(headers)
                .log().all()
                .when()
                .get(url)
                .then()
                .log().all()
                .extract()
                .response();

        return validateResponse(response, statusCode);
    }

    /**
     * Builds a POST rest call with form data
     *
     * @param url        Request Url
     * @param headers    Request headers
     * @param statusCode Response expected status code
     * @return {@link Response} response
     */
    protected Response postDefaultRestCallWithFormData(String url, Map<String, Object> headers, Map<String, String> bodyMap, Matcher<Integer> statusCode) {
        setupRestAssureLogCapture();
        RequestSpecification request = given()
                .config(RestAssured.config().logConfig(new LogConfig(printStream, true)))
                .spec(buildRequestBaseUri())
                .contentType("multipart/form-data")
                .headers(headers);

        for (Map.Entry<String, String> body : bodyMap.entrySet()) {
            request = request.multiPart(body.getKey(), body.getValue());
        }
        final Response response = request
                .log().all()
                .when()
                .post(url)
                .then()
                .log().all()
                .extract()
                .response();

        return validateResponse(response, statusCode);
    }

    private void setupRestAssureLogCapture() {
        stringWriter = new StringWriter();
        printStream = new PrintStream(new WriterOutputStream(stringWriter, Charset.defaultCharset()), true);
    }

    protected RequestSpecification buildRequestBaseUri() {
        ConfigProperty.RestProperties restProperties = ConfigPropertyReader.getConfigProperty().getRestProperties();
        return (new RequestSpecBuilder()).setBaseUri(restProperties.getRestBaseUrl()).build();
    }

    /**
     * Validates the response by checking statusCode. Throw {@link AssertionError} with REST call if mismatch
     *
     * @param response          Response
     * @param statusCodeMatcher Response expected status code
     * @return {@link Response} Response
     */
    private Response validateResponse(Response response, Matcher<Integer> statusCodeMatcher) {
        final String restCallLogOutput = stringWriter.toString();
        logger.debug(restCallLogOutput);

        final int responseStatusCode = response.getStatusCode();

        String errorMessage;

        if (!statusCodeMatcher.matches(responseStatusCode)) {
            final Description matcherResultDescription = new StringDescription().appendText("Expected: ")
                    .appendDescriptionOf(statusCodeMatcher);
            statusCodeMatcher.describeMismatch(responseStatusCode, matcherResultDescription);

            errorMessage = String.format("%s for REST call:\n%s", matcherResultDescription.toString(), restCallLogOutput);
            throw new AssertionError(errorMessage);
        }
        return response;
    }
}

