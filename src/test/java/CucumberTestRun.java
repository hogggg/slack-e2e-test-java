import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/",
        glue = "stepsDefinition",
        plugin = {"pretty",
                "html:target/cucumberReports/cucumber.html",
                "json:target/cucumberReports/cucumber.json",
                "junit:target/cucumberReports/cucumber.xml",
                "rerun:target/cucumberReports/rerun.txt"},
        tags = "@webReady")
public class CucumberTestRun {
}
