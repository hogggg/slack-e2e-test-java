package stepsDefinition;

import com.google.common.base.Splitter;
import config.ConfigPropertyReader;
import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import platform.rest.MessageRequestsService;
import platform.web.driver_manager.DriverManager;
import platform.web.driver_manager.DriverManagerFactory;
import platform.web.driver_manager.DriverType;
import platform.web.page.LoginPage;
import platform.web.page.MainPage;
import platform.web.page.SavedMessagePage;
import platform.web.page.SearchResultPage;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MessageStepsDefinition implements En {

    private static final Logger log = LogManager.getLogger(MessageStepsDefinition.class);
    private static final DriverType expectedWebBrowser = ConfigPropertyReader.getConfigProperty().getWebProperties().getBrowserType();

    public MessageStepsDefinition() {

        DriverManager driverManager = DriverManagerFactory.getManager(expectedWebBrowser);
        WebDriver driver = driverManager.getDriver();
        LoginPage loginPage = new LoginPage(driver);
        MainPage mainPage = new MainPage(driver);
        SavedMessagePage savedMessagePage = new SavedMessagePage(driver);
        SearchResultPage searchResultPage = new SearchResultPage(driver);

        Before((Scenario scenario) -> log.info("*********************************** <{}: {}> is running ***********************************",
                getFeatureNameByScenarioId(scenario.getId()), scenario.getName()));

        After((Scenario scenario) -> {
            log.info("*********************************** Start to clean up ***********************************");
            if (scenario.isFailed()) {
                //Screenshots are attached in the test result if any scenario is failed
                String screenshotName = scenario.getName().replaceAll(" ", "_");
                scenario.attach((((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)), "image/png", screenshotName);
            }
            driverManager.quitDriver();
        });

        Given("^Clear message history from channel (.*)$", (String channelName) -> {
            MessageRequestsService messageRequestsService = new MessageRequestsService();
            Optional<String> channelId = messageRequestsService.getChannelListFromSlackTeam().jsonPath()
                    .getList("channels", HashMap.class).stream().filter(channel -> channelName.equals(channel.get("name")))
                    .map(channel -> channel.get("id").toString()).findFirst();

            channelId.ifPresent(chId -> {
                List<String> messageTsList = messageRequestsService.retrieveChannelChats(chId).stream()
                        .map(msg -> msg.get("ts").toString()).collect(Collectors.toList());
                messageTsList.forEach(msgTs -> messageRequestsService.deleteChannelChat(chId, msgTs));
            });
        });

        When("^Navigate to slack workspace SlackTest$", loginPage::loginToSlack);

        And("^Send a message (.*) to Channel general$", mainPage::sendMessageToChannel);

        And("^Save the message (.*)$", mainPage::saveMessage);

        Then("^Go to Saved Items List on the channel sidebar$", mainPage::goToSaveItemsList);

        And("^Message (.*) appears in the saved items list$", (String message) -> Assert.assertEquals(message, savedMessagePage.getSavedMessage()));

        Then("^Search saved message from the search field$", () -> mainPage.searchMessage("has:star"));

        And("^Message (.*) appears in the search results$", (String message) -> Assert.assertEquals(message, searchResultPage.getSearchedMessage()));
    }

    private String getFeatureNameByScenarioId(String scenarioId) {
        return Splitter.on(" - ").trimResults().splitToList(scenarioId).get(0).replaceFirst(".*/(\\w+).feature:\\d+", "$1");
    }
}
