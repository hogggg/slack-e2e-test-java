Feature: Message

  Background:
    Given Clear message history from channel general

  @webReady
  Scenario: As a regular user, I'm able to search my saved slack messages
    When Navigate to slack workspace SlackTest
    And Send a message Hello World to Channel general
    And Save the message Hello World
    Then Go to Saved Items List on the channel sidebar
    And Message Hello World appears in the saved items list
    Then Search saved message from the search field
    And Message Hello World appears in the search results